import torch
import torchvision
from torch.utils.data import DataLoader, SequentialSampler, SubsetRandomSampler
from torchvision.datasets import ImageFolder
from torchvision.datasets.utils import download_url, check_integrity
from torchvision.transforms import transforms

#from new_ImageFolder import ImageFolder
import numpy as np
import albumentations as albu


from utils import BalancedBatchSampler

def get_training_augmentation():
    train_transform = [
     #albu.IAAAdditiveGaussianNoise(p=0.5),
     #albu.IAAAdditiveGaussianNoise(p=0.5),
     albu.ShiftScaleRotate(p=0.5),
     #albu.IAAPerspective(p=0.5),
    albu.OneOf([
        albu.HorizontalFlip(),
        albu.VerticalFlip(),
    ], p=1),

     albu.Rotate(p=0.5),
     albu.RandomResizedCrop(32,32, scale=(0.7, 1.0),p=0.5),
     #albu.VerticalFlip(p=0.5), #ONEOF TODO!!
     #albu.ChannelShuffle(p=0.5),
     #albu.RandomGamma(p=0.5),
     #albu.RandomGridShuffle(grid=(3, 3), p=0.5),
     #albu.RandomRotate90(p=0.5),
    ]
    return albu.Compose(train_transform,p=0.5)

class AlbumentationToTorchvision:
    def __init__(self, compose):
        self.compose = compose

    def __call__(self, x):
        data = {"image": x}
        return self.compose(**data)['image']


class ToNumpy:
    def __call__(self, x):
        return np.array(x)


class MyDataset_plus_annotation(object):
    def __init__(self, batch_size, path, train_annotation, test_annotation):
        self.batch_size = batch_size
        self.path=path

        # transform = [torchvision.transforms.RandomRotation(30), torchvision.transforms.RandomHorizontalFlip(),
        #              torchvision.transforms.RandomVerticalFlip(),
        #              torchvision.transforms.ColorJitter(brightness=(0, 50), contrast=(0, 30), saturation=(0, 40),hue=(-0.5, 0.5)),
        #              torchvision.transforms.RandomResizedCrop(size=(28,28), scale=(0.9, 1.0))]

        train_transforms = transforms.Compose([
            transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.507, 0.487, 0.441], std=[0.267, 0.256, 0.276])
        ])

        # Normalize test set same as training set without augmentation
        test_transforms = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.507, 0.487, 0.441], std=[0.267, 0.256, 0.276])
        ])

        # train_dataset = ImageFolder(path+'/train', transform=train_transforms, annotation=train_annotation)
        # test_dataset = ImageFolder(path+'/test', transform=test_transforms, annotation=test_annotation)

        train_dataset = ImageFolder(path + '/train', transform=train_transforms)
        test_dataset = ImageFolder(path + '/test', transform=test_transforms)

        #train_idx = [i for i in range(len(train_dataset.samples))]


        # num_train = len(train_dataset.samples)
        # indices = list(range(num_train))
        # split = int(np.floor(0.025 * num_train))
        #
        # np.random.seed(np.random.randint(0, 1000))
        # np.random.shuffle(indices)
        # train_idx, valid_idx = indices[split:], indices[:split]
        # train_sampler = SubsetRandomSampler(train_idx)
        # test_sampler = SubsetRandomSampler(valid_idx)

        train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=16, pin_memory=True) #for each batch we have batch-size/class instances.
        test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True, num_workers=16, pin_memory=True)
        self.trainloader = train_loader
        self.testloader = test_loader
        self.num_classes = len(train_dataset.classes)
        print("Full Dataset size: ", len(train_dataset.samples),"Classes: ", self.num_classes, "Test loader set: ", len(test_dataset.samples))





__factory = {
    'MyDataset': MyDataset_plus_annotation,
}


def create(name, batch_size, path,train_annotation,test_annotation):
    if name not in __factory.keys():
        raise KeyError("Unknown dataset: {}".format(name))
    return __factory[name](batch_size, path,train_annotation,test_annotation)
